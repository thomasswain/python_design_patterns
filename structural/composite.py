class Component(object):
    """Abstract class"""

    def __init__(self, *args, **kwargs):
        """Intentionally blank in abstract class"""
        pass

    def component_function(self):
        """Intentionally blank in abstract class"""
        pass


class Child(Component):
    """Concrete class - representing the leaves of the tree"""
    def __init__(self, *args, **kwargs):
        """Initialise Child class with superclass constructor, and set name as 1st argument"""
        Component.__init__(self, *args, **kwargs)
        self.name = args[0]

    def component_function(self):
        """Print the name of child item"""
        print("{}".format(self.name))


class Composite(Component):
    """Concrete class and maintains the tree recursive structure - represents a tree"""
    def __init__(self, *args, **kwargs):
        """Initialise Composite class with superclass constructor, and set name as 1st argument"""
        Component.__init__(self, *args, **kwargs)
        self.name = args[0]
        self.children = []

    def append_child(self, child):
        """Add a new child item"""
        self.children.append(child)

    def remove_child(self, child):
        """Remove chile item"""
        self.children.remove(child)

    def component_function(self):
        """Print name of composite, and call component_function of all children"""
        print("{}".format(self.name))
        for i in self.children:
            i.component_function()


# Create Composite submenu1
submenu1 = Composite("submenu1")

# Create 2 Children of Composite submenu1
sub11 = Child("submenu1_child1")
sub12 = Child("submenu1_child2")

# Add 2 Children to Composite submenu 1
submenu1.append_child(sub11)
submenu1.append_child(sub12)

# Create Top-level Composite topmenu
top = Composite("topmenu")

# Create Child submenu2 (not a Composite)
submenu2 = Child("submenu2")

# Add Composite submenu1 to topmenu
top.append_child(submenu1)
top.append_child(submenu2)

# Examine composite pattern with component_function on top-level Composite
top.component_function()

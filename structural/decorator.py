from functools import wraps


def make_blink(function):
    """make_blink docstring"""
    # This makes the decorator transparent in terms of its name and docstring - experiment commenting line below
    @wraps(function)
    # Define the inner function
    def decorator():
        """make_blink inner-function decorator docstring"""
        # Grab the return value of the function being decorated
        ret = function()
        # Add new functionality to the function being decorated
        return "<blink>" + ret + "</blink>"
    # Below must match name of inner function, without ()
    return decorator


# Apply the decorator here
@make_blink
def hello_world():
    """hello_world docstring"""
    return "Hello, World!"


# Check the result of decorating
print(hello_world())

# Check if the function name is still the same name of the function being decorated
print(hello_world.__name__)

# Check if the docstring is still the same as that of the function being decorated
print(hello_world.__doc__)

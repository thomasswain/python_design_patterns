class Korean:
    """Korean speaker"""
    def __init__(self):
        """Initialise Korean object with name Korean"""
        self.name = "Korean"

    def speak_korean(self):
        """Greeting in Korean"""
        return "An-neyong?"


class British:
    """English speaker"""
    def __init__(self):
        """Initialise British object with name British"""
        self.name = "British"

    def speak_english(self):
        """Greeting in English"""
        return "Hello"


class Adapter:
    """This changes the generic method name to individualized method names"""
    def __init__(self, o, **adapted_method):
        """Change the name of the method"""
        self._object = o
        self.__dict__.update(adapted_method)

    def __getattr__(self, attr):
        """Simply return the rest of the attributes"""
        return getattr(self._object, attr)


# List to store speaker objects
objects = []

# Create a Korean object
korean = Korean()

# Create a British object
british = British()

# Append objects to list
objects.append(Adapter(korean, speak=korean.speak_korean))
objects.append(Adapter(british, speak=british.speak_english))

# For each object in list, confirm individualised methods are being invoked
for obj in objects:
    print("{} says {}".format(obj.name, obj.speak()))

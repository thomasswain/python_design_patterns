class DrawingApiOne(object):
    """Implementation-specific abstraction: concrete class 1"""
    def draw_circle(self, x, y, radius):
        """Implementation 1 of draw_circle interface method"""
        print("API 1 drawing a circle at ({}, {}) with radius {}". format(x, y, radius))


class DrawingApiTwo(object):
    """Implementation-specific abstraction: concrete class 2"""
    def draw_circle(self, x, y, radius):
        """Implementation 2 of draw_circle interface method"""
        print("API 2 drawing a circle at ({}, {}) with radius {}". format(x, y, radius))


class Circle(object):
    """Implementation-independent abstraction: for example, there could be a rectangle class"""
    def __init__(self, x, y, radius, drawing_api):
        """Initialise the necessary attributes"""
        self._x = x
        self._y = y
        self._radius = radius
        self._drawing_api = drawing_api

    def draw(self):
        """Implementation-specific abstraction taken care of by some DrawingAPI class"""
        self._drawing_api.draw_circle(self._x, self._y, self._radius)

    def scale(self, percent):
        """Implementation-independent"""
        self._radius *= percent


# Create first Circle using API 1
circle1 = Circle(1, 2, 3, DrawingApiOne())

# Draw first Circle
circle1.draw()

# Create second Circle using API 2
circle2 = Circle(4, 5, 6, DrawingApiTwo())

# Draw second Circle
circle2.draw()
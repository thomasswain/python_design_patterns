import time


class Producer:
    """Represents a resource-intensive object to instantiate"""
    def produce(self):
        print("Producer is working hard")

    def meet(self):
        print("Producer has time to meet you now")


class Proxy:
    """Represents a relatively less resource-intensive proxy to instantiate as a middleman"""
    def __init__(self):
        """Initialise Proxy as unoccupied, without a Producer object"""
        self.occupied = "No"
        self.producer = None

    def produce(self):
        """Check if producer is available"""
        print("Artist checking if Producer is available")

        if self.occupied == "No":
            # If producer is available, create Producer object
            self.producer = Producer()
            time.sleep(2)
            # Make the producer meet the guest
            self.producer.meet()
        else:
            time.sleep(2)
            print("Producer is busy")


# Instantiate Proxy
proxy = Proxy()

# Make the Proxy repeatedly check if Producer is available
proxy.produce()

# Change state to occupied
proxy.occupied = "Yes"

# Make the Producer produce
proxy.produce()

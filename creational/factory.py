class Dog:
    """Simple dog class"""
    def __init__(self, name):
        """Initialise Dog with name input"""
        self._name = name

    def speak(self):
        """Sound a dog makes"""
        return "Woof!"


class Cat:
    """Simple cat class"""
    def __init__(self, name):
        """Initialise Cat with name input"""
        self._name = name

    def speak(self):
        """Sound a cat makes"""
        return "Meow!"


def get_pet(pet="dog"):
    """The factory method"""
    pets = dict(dog=Dog("Hope"), cat=Cat("Peace"))
    return pets[pet]


# Use factory to create Dog instance
d = get_pet("dog")
print(d.speak())

# Use factory to create Cat instance
c = get_pet("cat")
print(c.speak())

class Dog:
    """Represents a Dog"""

    def speak(self):
        """Sound a dog makes"""
        return "Woof"

    def __str__(self):
        """Text representation of a Dog object"""
        return "Dog"


class Cat:
    """Represents a Cat"""
    def speak(self):
        """Sound a cat makes"""
        return "Meow"

    def __str__(self):
        """Text representation of a Cat object"""
        return "Cat"


class DogFactory:
    """Concrete implementation of Factory - Dog"""
    def get_pet(self):
        """Returns a dog object"""
        return Dog()

    def get_food(self):
        """Returns Dog Food"""
        return "Dog Food"


class CatFactory:
    """Concrete implementation of Factory - Cat"""
    def get_pet(self):
        """Returns a cat object"""
        return Cat()

    def get_food(self):
        """Returns Cat Food"""
        return "Cat Food"


class PetStore:
    """PetStore houses our Abstract Factory"""
    def __init__(self, pet_factory=None):
        """pet_factory is our Abstract Factory"""
        self._pet_factory = pet_factory

    def show_pet(self):
        """Util method to display details of objects returned by DogFactory"""
        pet = self._pet_factory.get_pet()
        pet_food = self._pet_factory.get_food()

        print("Our pet is '{}'".format(pet))
        print("Our pet says hello by '{}'".format(pet.speak()))
        print("Its food is '{}'".format(pet_food))


# Create a Concrete Dog Factory
factory1 = DogFactory()

# Create a pet store housing our Abstract Factory
shop1 = PetStore(factory1)

# Show details of pet created by factory
print("First pet:")
shop1.show_pet()

print("")

# Create a Concrete Cat Factory
factory2 = CatFactory()

# Create a pet store housing our Abstract Factory
shop2 = PetStore(factory2)

# Show details of pet created by factory
print("Second pet:")
shop2.show_pet()

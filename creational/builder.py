class Director:
    """Represents director - input is the choice of concrete builder to use"""
    def __init__(self, builder):
        """Initialise Director with Builder from input arg"""
        self._builder = builder

    def construct_car(self):
        """Use Builder input to create specific Car and set attributes"""
        self._builder.create_new_car()
        self._builder.add_model()
        self._builder.add_tires()
        self._builder.add_engine()

    def get_car(self):
        """Get Car object constructed by Builder"""
        return self._builder.car


class Builder:
    """Abstract Builder"""
    def __init__(self):
        """Initialise Builder with empty Car"""
        self.car = None

    def create_new_car(self):
        """Create new Car object"""
        self.car = Car()


class SkyLarkBuilder(Builder):
    """Concrete Builder - provides parts and tools to create SkyLark Car"""
    def add_model(self):
        """Set model for SkyLark"""
        self.car.model = "Skylark"

    def add_tires(self):
        """Set tires for Skylark"""
        self.car.tires = "Regular tires"

    def add_engine(self):
        """Set engine for Skylark"""
        self.car.engine = "Turbo engine"


class BatmobileBuilder(Builder):
    """Concrete Builder - provides parts and tools to create Batmobile Car"""
    def add_model(self):
        """Set model for BatMobile"""
        self.car.model = "Batmobile"

    def add_tires(self):
        """Set tires for Batmobile"""
        self.car.tires = "Bat tires"

    def add_engine(self):
        """Set engine for Batmobile"""
        self.car.engine = "Bat engine"


class Car:
    """Product"""
    def __init__(self):
        """Initialise Car with empty attributes"""
        self.model = None
        self.tires = None
        self.engine = None

    def __str__(self):
        """Text representation of Car's attributes"""
        return "{} | {} | {}".format(self.model, self.tires, self.engine)


builder1 = SkyLarkBuilder()
director1 = Director(builder1)
director1.construct_car()
car1 = director1.get_car()
print("First car")
print(car1)

print("")

builder2 = BatmobileBuilder()
director2 = Director(builder2)
director2.construct_car()
car2 = director2.get_car()
print("Second car")
print(car2)

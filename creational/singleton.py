class Borg:
    """Borg class making class attributes global"""
    _shared_state = {}

    def __init__(self):
        """Initialise Borg class by creating attribute dictionary from """
        self.__dict__ = self._shared_state


class Singleton(Borg):
    """This class now shares all its attributes among its various instances"""
    def __init__(self, **kwargs):
        """Initialise Singleton by calling superclass constructor and updating shared_state with input args"""
        Borg.__init__(self)
        self._shared_state.update(kwargs)

    def __str__(self):
        """Text representation of Singleton's shared_state"""
        return str(self._shared_state)


# Create a singleton object and add our first acronym
x = Singleton(HTTP="Hyper Text Transfer Protocol")

# Print the object
print(x)

# Create another singleton object and it refers to the same attribute dictionary by adding another acronym
y = Singleton(SNMP="Simple Network Management Protocol")

# Print the object
print(y)

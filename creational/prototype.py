import copy


class Prototype:
    """Class used to register and clone an object"""
    def __init__(self):
        """Initialist Prototype with empty dictionary"""
        self._objects = {}

    def register_object(self, name, obj):
        """Register object in dictionary"""
        self._objects[name] = obj

    def unregister_object(self, name):
        """Unregister object by deleting from dictionary"""
        del self._objects[name]

    def clone(self, name, **attr):
        """Clone object and optionally change attributes"""
        obj = copy.deepcopy(self._objects.get(name))
        obj.__dict__.update(attr)
        return obj


class Car:
    """Represents a Car"""
    def __init__(self):
        """Initialise Car object with Skylark attributes by default"""
        self.model = "Skylark"
        self.color = "Red"
        self.options = "Ex"

    def __str__(self):
        """Text representation of Car's attributes"""
        return "{} | {} | {}".format(self.model, self.color, self.options)


# Make original Skylark car
car = Car()
print(car)

# Register original car in Prototype to allow cloning
prototype = Prototype()
prototype.register_object("skylark", car)

# Clone the Skylark car
cloned_car1 = prototype.clone("skylark")
print(cloned_car1)

# Clone the Skylark car again, but changing Car attributes
cloned_car2 = prototype.clone("skylark", model="Batmobile", color="Black", options="Curved")
print(cloned_car2)

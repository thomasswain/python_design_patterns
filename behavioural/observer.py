class Subject:
    """Represents what is being observed"""
    def __init__(self):
        """Initialise with empty list, holding references to all observers"""
        self._observers = []

    def attach(self, observer):
        """Appends observer to list of observers, if not already in the list"""
        if observer not in self._observers:
            self._observers.append(observer)

    def detach(self, observer):
        """Remove observer from list of observers, if already present"""
        try:
            self._observers.remove(observer)
        except ValueError:
            pass

    def notify(self, modifier=None):
        """Alert all observers in the list, except the one who updated temperature. Incomplete - modifier does nothing"""
        for observer in self._observers:
            if modifier != observer:
                observer.update(self)


class Core(Subject):
    """Inherits from Subject class"""
    def __init__(self, name=""):
        """Call superclass constructor, set name to input, set temp to 0"""
        Subject.__init__(self)
        self._name = name
        self._temp = 0

    @property
    def temp(self):
        """Getter for temp"""
        return self._temp

    @property
    def name(self):
        """Getter for temp"""
        return self._name

    @temp.setter
    def temp(self, temp):
        """Setter for temp, notify observers when changed"""
        self._temp = temp
        self.notify()


class Observer:
    """The observer of temperature"""
    def update(self, subject):
        """Invoked when the notify() method in concrete subject is invoked"""
        print("Notification for {} - temperate is now {} degC".format(subject.name, subject.temp))


# Create 2 Cores
core1 = Core("Core 1")
core2 = Core("Core 2")

# Create 2 Observers
observer1 = Observer()
observer2 = Observer()

# Attach Observers to Cores
core1.attach(observer1)
core1.attach(observer2)

# Change temperature
core1.temp = 80
core1.temp = 90

def count_to(count):
    """Our iterator implementation"""

    # List
    numbers_in_german = ["eins", "zwei", "drei", "vier", "funf"]

    # Our built-in iterator - creates tuples such as (1, "eins")
    iterator = zip(range(count), numbers_in_german)

    # Iterate through our list, extract German number, put them in generator called number
    for position, number in iterator:
        yield number


# Test generator returned by our iterator
# Note for argument>5 no error in output - strength of iterator
for num in count_to(6):
    print("{}".format(num))

import types


class Strategy:
    """Holds execute method, which can be varied depending on strategy desired"""
    def __init__(self, function=None):
        """If a function is provided as input, execute that function"""
        self.name = "Default Strategy"
        if function:
            self.execute = types.MethodType(function, self)

    def execute(self):
        """Default execute method prints the name of the strategy being used"""
        print("{} is used".format(self.name))


def strategy_one(self):
    """Replacement method 1"""
    print("{} is used instead of default execute method".format(self.name))


def strategy_two(self):
    """Replacement method 2"""
    print("{} is used instead of default execute method".format(self.name))


# Create default strategy
s0 = Strategy()

# Execute default strategy
s0.execute()

# Create first variation of default strategy
s1 = Strategy(strategy_one)
s1.name = "Strategy One"

# Execute first variation
s1.execute()

# Create second variation of default strategy
s2 = Strategy(strategy_two)
s2.name = "Strategy Two"

# Execute second variation
s2.execute()

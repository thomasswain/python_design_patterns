class House:
    """The class being visited"""
    def accept(self, visitor):
        """Interface to accept a visitor"""
        visitor.visit(self)

    def work_on_hvac(self, hvac_specialist):
        """House object has reference now to HVAC specialist"""
        print("{} worked on by {}".format(self, hvac_specialist))

    def work_on_electricity(self, electrician):
        """House object has reference now to electrician"""
        print("{} worked on by {}".format(self, electrician))

    def __str__(self):
        """Return House class name"""
        return self.__class__.__name__


class Visitor:
    """Abstract visitor"""
    def __str__(self):
        """Return Visitor class name"""
        return self.__class__.__name__


class HvacSpecialistVisitor(Visitor):
    """Concrete Visitor - HVAC Specialist"""
    def visit(self, house):
        """Visit House object in input and call relevant work method"""
        house.work_on_hvac(self)


class Electrician(Visitor):
    """Concrete Visitor - Electrician"""
    def visit(self, house):
        """Visit House object from input and call relevant work method"""
        house.work_on_electricity(self)


# Create a HVAC Speciailist
hvac = HvacSpecialistVisitor()

# Create an Electrician
elec = Electrician()

# Create a House
h = House()

# Let the House accept HVAC Specialist and start work on the House
h.accept(hvac)

# Let the House accept Electrician and start work on the House
h.accept(elec)

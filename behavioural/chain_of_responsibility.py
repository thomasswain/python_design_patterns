class Handler:
    """Abstract Handler"""
    def __init__(self, successor=None):
        """Define who is the next Handler"""
        self._successor = successor

    def handle(self, request):
        """Handle incoming request - if handled stop here, otherwise handle"""
        handled = self._handle(request)
        if not handled:
            self._successor.handle(request)

    def _handle(self, request):
        """Raises error if no concrete implementation exists"""
        raise NotImplementedError("Must provide implementation in subclass")


class ConcreteHandler1(Handler):
    """Concrete Handler 1"""
    def _handle(self, request):
        """Override superclass method - able to handle requests between 0 and 10"""
        if 0 < request <= 10:
            print("Request {} handled in handler 1".format(request))
            return True
        else:
            print("Request {} NOT handled in handler 1".format(request))
            return False


class ConcreteHandler2(Handler):
    """Concrete Handler 2"""
    def _handle(self, request):
        """Override superclass method - able to handle requests greater than 10"""
        if 10 < request:
            print("Request {} handled in handler 2".format(request))
            return True
        else:
            print("Request {} NOT handled in handler 2".format(request))
            return False


class DefaultHandler(Handler):
    """Concrete Handler Default"""
    def _handle(self, request):
        """If there is no handle available"""
        print("End of chain, no handler for {}".format(request))
        return True


class Client:
    """Makes use of Handlers"""
    def __init__(self):
        """Create handlers and use them in a specific sequence"""
        self.handler = ConcreteHandler1(ConcreteHandler2(DefaultHandler()))

    def delegate(self, requests_for_handlers):
        for request in requests_for_handlers:
            self.handler.handle(request)


# Create Client
c = Client()

# Create requests
requests = [2, 5, 15, 30]

# Delegate the requests to handlers
c.delegate(requests)
